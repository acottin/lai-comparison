#!/usr/bin/env bash

#source number
K="3 4 5 6"
#admixed number
A="2"
#generation number
G="50"
#differentiation
D="0.20"
#sample size
S="05"

for VK in $K
do
  #total populatlion number
  P=$(expr $VK + $A)
  DIR="val_k${VK}"
  mkdir -p $DIR
      
  #reprod file
  rm -f "$DIR/reprod.tab"
  #total populatlion number
  (( P = $VK + $A))
  for I in $(seq 1 $P); do echo "0.0/0.0/1.0/0.0:$G" >> "$DIR/reprod.tab"; done

  #contrib file
  rm -f "$DIR/contrib.tab"
  # source contrib
  for i in $(seq 1 $VK)
  do
    MLINE=""
    for j in $(seq 1 $VK)
    do
      #diagonal at 300
      if [ $i == $j ]
      then 
        MLINE="${MLINE}300"
      else
        MLINE="${MLINE}0"
      fi
      #if end of line, print
      if [ $j == $VK ]
      then
        echo $MLINE >> "$DIR/contrib.tab"
      #else add white space
      else
        MLINE="$MLINE "
      fi
    done
  done
  (( CNTRB = 300/$VK ))
  for i in $(seq 1 $A)
  do
    MLINE=""
    for j in $(seq 1 $VK)
    do
      MLINE="${MLINE}${CNTRB}"
      if [ $j == $VK ]
      then 
        echo $MLINE >> "$DIR/contrib.tab"
      else
        MLINE="$MLINE "
      fi
    done
  done

  cat > "$DIR/param.R" << EOL
# foundation
n_init_hap <- 1e3
n_hap_per_pop <- rep(3e2,$VK)
mut_rate <- 1e-8
chr_size <- 1e6 #1M
# reproduction steps
time_split <- rep($D,$VK-1)
n_sample_per_pop <- c(rep($S,$VK),40,40)
pop_tag <- c(seq($VK),rep("admx",$A))
EOL
done
