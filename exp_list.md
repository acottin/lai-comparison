exp_1: DiffGenSam
-----

fixed param:

- 3 source
- 2 admixed population (1/3 of each source)
- 20 samples from each source-rep pop
- 40 samples from each admixed pop

variable : source diff (tau),source sample size (op-src), generation number after admixture(G)

* tau: 0.05,0.1,0.2,0.3,0.4
* G: 5,10,20,30,40,50
* op-src: 5,10,15,20,30,40


exp_2: SrcNum
-----

"more than 3 sources"

fixed param:

- 2 admixed population (1/S of each source)
- 5 samples from each source-rep
- 40 samples from each admixed pop
- 50 generations after admixture
- tau = 0.2

variable : source number (S)

* S: 4,5,6

exp_3: SrcMiss
-----

"missing source"

fixed param: 

- 4 sources
- 2 admixed pop (1/4 of *the 3 first source*)
- 20 samples from each source-rep
- 40 samples from each admixed
- 50 generations
- tau = 0.2
- 20 sample from *the 3 first source* source

variable : percentage of missing pop (Un)

* Un : 0.05 0.10 0.15

exp_4: SamBal
-----

"unbalanced source sample size"

fixed param:

- 3 sources
- 2 admixed population (1/3 of each source)
- 20 samples from the *first two source*
- 40 samples from the two admixed
- 50 generations after admixture
- tau = 0.2

variable : number of individual for third source (op-src3)

* op-src3: 2,4,6,8,10,15,20

exp_5: SrcSelf
-----

"selfing in one source population"

fixed param:

- 3 sources
- 1 split event
- 1 admixture pulse
- 2 admixed population (1/3 of each source)
- 20 samples from the source population
- 40 samples from the two admixed
- 50 generations after admixture
- tau = 0.2

variable : selfing proportion of the third source

* Slf: 0,0.25,0.50,0.75,0.99

exp_6: AdmxVegProp
-----

"vegetative prop"

fixed param:

- 3 sources
- 10 admixed population (1/3 of each source)
- 20 samples from the source population
- 10 samples from the two admixed
- 50 generations after admixture
- tau = 0.2

variable : number of generation of veg propag for the admixed pop

* ?: 0,5,10,15,20,25,30,35,40,45
