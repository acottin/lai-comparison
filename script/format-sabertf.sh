#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "vcftools plink admixture"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

source $DIR_IN/out_param.sh
#get raw file to output directory
vcftools --vcf $DIR_IN/data.vcf --012 --out $DIR_OUT/data
vcftools --vcf $DIR_IN/data.vcf --plink --out $DIR_OUT/data
#move in then finish formatting
cd $DIR_OUT

# 012 file
#position from column to rows (replace newline by tab), replace trailing tab by new line
cut -f 2 data.012.pos | tr "\n" "\t" | sed 's/\t$/\n/' > position.tmp
#remove first line from genotype and concatenate it to position
cut -f 1 --complement data.012 | cat position.tmp - > data.saber_input
#then remove tmp
rm position.tmp

# Q matrix (using admixture)
#convert to ped
plink --file data --recode12 --out data
#run admixture
admixture --cv=10 data.ped $N_ANC
