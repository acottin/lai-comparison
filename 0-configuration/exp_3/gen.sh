#!/usr/bin/env bash

#source number
K="4"
#admixed number
A="2"
#total populatlion number
P=$(expr $K + $A)
#generation number
G="50"
#differentiation
D="0.20"
#sample size
S="20"
#missing pop proportion in admixed
M="0.00 0.05 0.10 0.15"

for VM in $M
do
  #directory
  DIR="val_m${VM}"
  mkdir -p $DIR
      
  #reprod file
  rm -f "$DIR/reprod.tab"
  for I in $(seq 1 5); do echo "0.0/0.0/1.0/0.0:$G" >> "$DIR/reprod.tab"; done

  #contrib file
  rm -f "$DIR/contrib.tab"
  cat > "$DIR/contrib.tab" << EOL
300 0 0 0
0 300 0 0
0 0 300 0
EOL
  SPROP=$(echo "(300 - (300 * $VM))/3" | bc)
  APROP=$(echo "(300 - $SPROP * 3)" | bc)

  for i in $(seq 1 $A)
  do
    echo "$SPROP $SPROP $SPROP $APROP" >> "$DIR/contrib.tab"
  done

cat > "$DIR/param.R" << EOL
# foundation
n_init_hap <- 1e3
n_hap_per_pop <- rep(3e2,$K)
mut_rate <- 1e-8
chr_size <- 1e6 #1M
# reproduction steps
time_split <- c($D,$D,$D)
n_sample_per_pop <- c($S,$S,$S,40,40)
pop_tag <- c(seq($K-1),rep("admx",$A))
EOL

done
