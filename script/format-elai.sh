#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "plink" "vcftools"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

#split vcf
for POP_FILE in $DIR_IN/pop_*.txt
do
    PREFIX=$(basename $POP_FILE .txt)
    OUT=$DIR_OUT/$PREFIX
    vcftools --vcf $DIR_IN/data.vcf --keep $POP_FILE --out $OUT --recode --recode-INFO-all
done

cd $DIR_OUT
#remove 'recode' from file name
rename '.recode' '' *.recode.*

#vcf to bimbam
for VCF_FILE in pop_*.vcf
do
    OUT=$(basename $VCF_FILE .vcf)
    plink -vcf $VCF_FILE --out $OUT --recode bimbam #--set-missing-var-ids '@:#' 
done

#clean output files
rm *.nosex *.pheno.txt
rename '.recode' '' *.recode.*
rename '.txt' '' *.txt
rm pop_*.vcf
rm pop_*.log*
