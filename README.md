# Local ancestry inference software comparison by simulation

This repository contains the code used to:
1. run simulations
2. format the data for SABER (Tang *et al*, 2006) WINPOP (Paşaniuc *et al*, 2009) and ELAI (Guan, 2014)
3. run each LAI software
4. format their output consistently

## Files descriptions

### Bash scripts (in the project root)

#### Script `conf.sh`

Contains variables that set the names of directories for each part of the study.
* `CON`: directory `0-configuration` containing config file
* `SIM`: directory `1-simulation` containing simulated data
* `INF`: directory `2-inference` containing inference-ready data
* `DAT`: directory `3-data` containing inferred and formated data
* `ANA`: directory `4-analysis` containing comparison data (between truth and inference)

The associative array `VALUES` contains each configuration file path from each simulations (noted from 1 to 6).

`SGE_Q` contains the queue used for jobs from the southgreen cluster.

`METHODS` contains the software used for the study (here, saber, elai and lamp).

`EXPERIENCES` contains the keys from `VALUES`.

`REP_NUM` is used to create a list of replicate identifiers (numbers from 1 to `REP_NUM` with leadings 0 if needed).

`REPLICATION` contains the list of replicate identifiers.

#### Scripts `deploy.sh` and `clean.sh`

`deploy.sh` construct the whole tree of directories to store simulation data, software input and output and formated data. It uses `conf.sh`.

`clean.sh` remove the whole tree and contained data.

#### Scripts `run-*.sh`

Each one of these files contains a script to run on the cluster one step of the study.
Keep in mind that they were designed rapidly to allow us to rerun part of simulations.


Notes : Scripts are not generic and wont work "as is" in other cluster environment. They always begin by sourcing `conf.sh`. The `module` command afterward are used to modify the environment to get the cited software. This is certainly not available in the same format on others clusters, therefore they should be commented. Something like `sed -i 's/module/#module/' run-*.sh` should do the work.

##### Script `run-1-simulation.sh`

Run all simulations using the `simulation.R`.
As input, it uses configurations files from the configuration directory.
Output are set in the simulation directory.
It needs access to R with the package `plmgg` installed.

##### Script `run-1-format.sh`

Run input formating step for each software.
It uses `format-*.sh` scripts from the `script/` directory.
Input are simulated datasets and metadata contained in the simulation directory.
Output are set in the inference directory.
It needs dependencies from each script used for formating, that are :
* `vcftools`
* `plink`
* `admixture`

##### Script `run-2-inference.sh`

Run each inference software on formated input.
It uses `run-*.sh` scripts from the `script/` directory.
Input are formated datasets from the inference directory.
Output are also set in the inference directory, they are the raw output from each program.
It needs `saber` package and a `R` environment, `elai` and `lamp` in `PATH`.

##### Script `run-3-data.sh`

Run output formating step for each software.
It uses `out-format-*.sh` scripts from the `script/` directory.
Input are inferred data and from the inference directory.
Output are set in the data directory.
It needs `bzip2` and `R`.

##### Script `run-4-comparison.sh`

Run evaluation script `eval.R` on each dataset, comparing inferred data with truth.
Random inference is also evaluated during this step. Multiple "random" inferrence were tested.
Input are inferred and formated data and truth from simulated data.
Output are tabulated files with a metric per method per individuals. Methods are columns, individuals are rows.

### Directory `script`

Contains script for each LAI software to run them and to do data formating (in
and out). These are all automatically run by the `run-*.sh` scripts. They all
check for needed software (e.g. `vcftools`) before running.

#### Script `format-*.sh`

Run a formatting step to get adapted data for each LAI software. Options are paths
of input (`-i`) and output (`-o`) directories.

#### Script `run-*.sh`

Run the software on the formatted data. Options are the working directory (`-d`)
and the configuration file (`-c`).

#### Script `out-format-*.sh`

Run a second formatting step to get simple tabulated data (one column per
ancestry, one line per marker). These files are bz2-ipped. Input Options are the
input and output directories (`-i`, `-o`) and the configuration file (`-c`).

## Notes

* SABER does not seem to be still available online. It was at the beginning of 
this works installed locally on and old version of R (2.15). I needed to 
remove the `zzz.R` file from the packages prior to installation.
* The script refering to Mosaic and fMosaic aims at a prototype software that 
are not linked to Mosaic from Salter-Townshed & Myers (2019). Script refering to
"vts" were used to test another software still unpublished.
* I apologize for the lack of re-usability of given scripts. This work support a 
PhD thesis which was ran with a focus not set on providing clear and usable pieces
of software but rather on how the LAI software were usable for a biological study,
and the biological study itself. With given time (after the PhD defence), I will
try to wrap all of it in a pipeline framework like snakemake or nextflow.

## Bibliography

* Tang, H., Coram, M., Wang, P., Zhu, X., & Risch, N. (2006). Reconstructing Genetic Ancestry Blocks in Admixed Individuals. The American Journal of Human Genetics, 79(1), 1–12. https://doi.org/10.1086/504302
* Paşaniuc, B., Sankararaman, S., Kimmel, G., & Halperin, E. (2009). Inference of locus-specific ancestry in closely related populations. Bioinformatics, 25(12), i213–i221. https://doi.org/10.1093/bioinformatics/btp197
* Guan, Y. (2014). Detecting structure of haplotypes and local ancestry. Genetics, 196(3), 625–642. https://doi.org/10.1534/genetics.113.160697
* Salter-Townshend, M., & Myers, S. (2019). Fine-Scale Inference of Ancestry Segments Without Prior Knowledge of Admixing Groups. Genetics, 212(3), 869–889. https://doi.org/10.1534/genetics.119.302139