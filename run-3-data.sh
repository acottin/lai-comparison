#!/bin/bash
source "conf.sh"

module purge
module load bioinfo/R/3.4.3
module load system/python/3.4.3

local_Q="short.q"

for EXP in $EXPERIENCES
do
  for VAL in ${VALUES[$EXP]}
  do
    for REP in $REPLICATION
    do
      for MET in $METHODS
      do
        JHOLD="$MET-$EXP-$VAL-$REP"
        JN="format_out-$MET-$EXP-$VAL-$REP"
        CFG="$SIM/exp_$EXP/val_$VAL/rep_$REP/out_param.sh"
        IN="$INF/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
        OUT="$DAT/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
        case $MET in
          elai)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-elai.sh -i $IN -o $OUT -c $CFG"
            ;;
          saber)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-saber.sh -i $IN -o $OUT -c $CFG"
            ;;
          sabertf)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-sabertf.sh -i $IN -o $OUT -c $CFG"
            ;;
          fmosaic)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-mosaic.sh -i $IN -o $OUT -c $CFG"
            ;;
          mosaic)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-mosaic.sh -i $IN -o $OUT -c $CFG"
            ;;
          vts)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-vts.sh -i $IN -o $OUT -c $CFG"
            ;;
          lamp)
            qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/out-format-lamp.sh -i $IN -o $OUT -c $CFG"
            ;;
        esac
      done
    done
  done
done

