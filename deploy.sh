#!/bin/bash

#http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

source "conf.sh"

TO_PRINT="EXP VAL REP MET"
for EXP in $EXPERIENCES
do
  for MET in $METHODS
  do
    for VAL in ${VALUES[$EXP]}
    do
      for REP in $REPLICATION
      do
        TO_PRINT+="\n$EXP $VAL $REP $MET" 
        mkdir -p "$CON/exp_$EXP/val_$VAL"
        mkdir -p "$SIM/exp_$EXP/val_$VAL/rep_$REP"
        mkdir -p "$INF/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
        mkdir -p "$DAT/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
        mkdir -p "$ANA"
      done
    done
  done
done

#print
echo -e $TO_PRINT > list.txt
