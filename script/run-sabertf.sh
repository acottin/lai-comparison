#!/usr/bin/env bash

#-----------
# Parameters

usage() {  echo "Usage: $0 [-d <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "d:c:" OPT
do
    case "$OPT" in
        d)
	    DIR=$OPTARG
	    ;;
	c)
	    CONF=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#----
# Run

source $CONF
cd $DIR

N_SRC=$(echo $SRC_POP_SIZE | sed "s/,/+/g" | bc)
N_ADMX=$(expr $N_IND - $N_SRC)

saber.R -i data.${N_ANC}.Q -G data.saber_input -n $SRC_POP_SIZE -N $N_ADMX -t $N_GEN -f
