#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in ""
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

source $DIR_IN/out_param.sh
#cd $DIR_OUT

ln -s $(readlink -e $DIR_IN/data.vcf) $DIR_OUT

N_SRC=$(echo $SRC_POP_SIZE | sed "s/,/+/g" | bc)

#names files
cat $DIR_IN/data.vcf | grep "#CHROM" | sed 's/\t/\n/g' | tail -n +10 > $DIR_OUT/all_names.txt
head -$N_SRC $DIR_OUT/all_names.txt > $DIR_OUT/ancestor_names.txt

#add [group] and [color] section
echo "[group]" > $DIR_OUT/group.tab

for FILE in $DIR_IN/pop_*.txt
do
  if [[ "$FILE" != *admx* ]]
  then
    POP_NUM=$( basename $FILE | egrep -o '[[:digit:]]+')
    awk -v p="$POP_NUM" '{ print $1 "\tgp" p }' $FILE >> $DIR_OUT/group.tab
  fi
done
