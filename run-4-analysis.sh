#!/bin/bash
source "conf.sh"

module purge
module load bioinfo/R/3.4.3

for EXP in $EXPERIENCES
do
  for VAL in ${VALUES[$EXP]}
  do
    for REP in $REPLICATION
    do
      DIR_LIST=""
      #TODO: separate software with need of switch
      DIR_LIST_SWITCH=""
      for MET in $METHODS
      do
        JHOLD=\''format_out-*'"-$EXP-$VAL-$REP"\'
        JN="analysis-$EXP-$VAL-$REP"
        ORI="$SIM/exp_$EXP/val_$VAL/rep_$REP/"
        CFG="$ORI/out_param.sh"
        IN="$DAT/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
	#
        #dont add empty directory to list of LAI output
        if [ "$(ls -A $IN)" ]
        then
          DIR_LIST="$DIR_LIST $IN"
        fi
      done
      source $CFG
      qsub -hold_jid $JHOLD -q normal.q -b yes -V -N $JN \
      eval.R -t $ORI -s $N_ANC -o $ANA -O "$EXP-$VAL-$REP" $DIR_LIST
    done
  done
done

