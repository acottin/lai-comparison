#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "i:o:c:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	c)
	    CONF_FILE=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "python3"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Output


source $CONF_FILE

/home/acottin/documents/lai-tools/format/vcf-to-struct_to_tab.py \
--dir-in "$DIR_IN/inf" \
--dir-out $DIR_OUT     \
--ploidy 2             \
--n-src $N_ANC

#now we need to remove the ancestors

N_SRC_IND=$(echo $SRC_POP_SIZE | tr ',' '+' | bc)

cd $DIR_OUT
COUNT=1
for F in sample*.tab
do
  if [ $COUNT -le $N_SRC_IND ]
  then
    rm $F
  fi
  COUNT=$(expr $COUNT + 1)
done
  
