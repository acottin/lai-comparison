#!/bin/bash

# initialization

CON="0-configuration"
SIM="1-simulation"
INF="2-inference"
DAT="3-data"
ANA="4-analysis"

declare -A VALUES

SGE_Q="normal.q"

METHODS="saber elai lamp"
METHODS="saber"

#extract all values from each experience
VALUES=(
  ["1"]=$(for I in 0-configuration/exp_1/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["2"]=$(for I in 0-configuration/exp_2/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["3"]=$(for I in 0-configuration/exp_3/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["4"]=$(for I in 0-configuration/exp_4/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["5"]=$(for I in 0-configuration/exp_5/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["6"]=$(for I in 0-configuration/exp_6/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
)

VALUES=(
#  ["1"]=$(for I in 0-configuration/exp_1/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
#  ["2"]=$(for I in 0-configuration/exp_2/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
#  ["3"]=$(for I in 0-configuration/exp_3/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
#  ["4"]=$(for I in 0-configuration/exp_4/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
#  ["5"]=$(for I in 0-configuration/exp_5/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
  ["6"]=$(for I in 0-configuration/exp_6/val_*; do echo $(basename $I) | sed 's/val_//g' | tr "\n" " " ; done)
)

EXPERIENCES="${!VALUES[*]}"

REP_NUM=50
REPLICATION=$(seq -w 1 $REP_NUM)
