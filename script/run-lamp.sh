#!/usr/bin/env bash

#-----------
# Parameters

usage() {  echo "Usage: $0 [-d <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "d:c:" OPT
do
    case "$OPT" in
        d)
	    DIR=$OPTARG
	    ;;
	c)
	    CONF=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#----
# Run

source $CONF
cd $DIR

lamp run.cfg
