#!/bin/bash
source "conf.sh"

module purge
module load bioinfo/R/3.4.3

for EXP in $EXPERIENCES
do
  for VAL in ${VALUES[$EXP]}
  do
    for REP in $REPLICATION
    do
      IN="$CON/exp_$EXP/val_$VAL"
      OUT="$SIM/exp_$EXP/val_$VAL/rep_$REP"
      qsub -q normal.q -b yes -V -N simu-${EXP}-${VAL}-${REP} -l h_vmem=16G  \
      ./simulation.R                 \
      --param-file   $IN/param.R     \
      --reprod-file  $IN/reprod.tab  \
      --contrib-file $IN/contrib.tab \
      --output-dir   $OUT            \
      --rng-seed     $REP
    done
  done
done

