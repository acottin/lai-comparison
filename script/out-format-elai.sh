#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "i:o:c:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	c)
	    CONF_FILE=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "R" "elai-ps21_to_tab.R"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Output

# choose file

PREFIX=$DIR_IN"/output/1"
FILE_LOG=${PREFIX}.log.txt
FILE_INF=${PREFIX}.snpinfo.txt
FILE_PS=${PREFIX}.ps21.txt

# get snp and individuals number (dont need to use conf file, all is in log)

N_SNP=$(grep "## number of snp " $FILE_LOG | grep -oP "\d+")
N_IND=$(grep "## number of cohort " $FILE_LOG | grep -oP "\d+")
N_SRC=$(grep "## COMMAND" $FILE_LOG | grep -oP "\-C \d+" | grep -oP "\d+")

#run

elai-ps21_to_tab.R -i $FILE_PS -I $FILE_INF -m $N_SNP -n $N_IND -s $N_SRC -o $DIR_OUT
