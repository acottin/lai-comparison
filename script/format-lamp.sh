#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "plink" "admixture" "vcftools"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

source $DIR_IN/out_param.sh
#get raw file to output directory
vcftools --vcf $DIR_IN/data.vcf --keep $DIR_IN/pop_admx.txt --012 --out $DIR_OUT/tmp
cut -f 1 --complement $DIR_OUT/tmp.012 > $DIR_OUT/data.geno
cut -f 2 $DIR_OUT/tmp.012.pos > $DIR_OUT/data.pos
rm $DIR_OUT/tmp.*
vcftools --vcf $DIR_IN/data.vcf --plink --out $DIR_OUT/data
PFILE=""
#we need to reverse --freq2 count (awk 1-$0)
for POP in $(seq $N_ANC)
do
    POP_NAME=pop_${POP}
    vcftools --vcf $DIR_IN/data.vcf --keep $DIR_IN/${POP_NAME}.txt --freq2 --out $DIR_OUT/ --stdout \
    | cut -f 5 \
    | tail -n +2 \
    | awk '{ print 1 - $0 }' \
    > $DIR_OUT/${POP_NAME}.freq.txt
    PFILE=$PFILE,${POP_NAME}.freq.txt
done
rm $DIR_OUT/data.log

#move in then finish formatting
cd $DIR_OUT

# Q matrix (using admixture)
#convert to ped
plink --file data --recode12 --out data
#run admixture
admixture --cv=10 data.ped $N_ANC
# now we need to reorder column because admixture mess with it.
# we want the first column to be the first origin, so we need to
# sort by row (only origins rows)

#source: https://stackoverflow.com/questions/25062169/using-bash-to-sort-data-horizontally#25062537
transpose () {
  awk '{
    for (i=1; i<=NF; i++){
      a[i,NR] = $i
      max = (max<NF?NF:max)
    }
  }
  END {
    for (i=1; i<=max; i++){
      for (j=1; j<=NR; j++) 
      printf "%s%s", a[i,j], (j<NR?OFS:ORS)
    }
  }'
}
N_PURE=$(echo $SRC_POP_SIZE | sed "s/,/+/g" | bc)

cat data.${N_ANC}.Q \
| transpose \
| sort -k 1,$N_PURE -n -r \
| transpose > tmp

tail -n +$(expr $N_PURE + 1) tmp  > data.${N_ANC}.Q

N_ADMX=$(expr $N_IND - $N_PURE)

#now we need a rounded mean of the data.?.Q file
#with awk:

awk '
{ 
  for(i=1;i<=NF;i++){
    arr[i]+=$i
  }
} 
END{
  #mean
  for(i=1;i<=NF;i++){
    mean[i] = arr[i]/NR
  }
  sum=0
  max=1
  f=3 #rounding factor
  for(i=1;i<=NF;i++){
    #round
    mean[i]=sprintf("%."f"f",mean[i])
    #get max
    if(mean[max] < mean[i]){
      max=i
    }
    #adjust if 0
    if(mean[i] < 10^-f){
      mean[i] =  1 * 10^-f
    }
    sum += mean[i]
  }
  #adjust max value to sum difference
  if(sum != 1){
    delta = sum - 1
    mean[max] -= delta
  }
  for(i=1;i<=NF;i++){
    printf "%."f"f%c", mean[i] , (i == NF) ? "\n" : " "
  }
}' "data.${N_ANC}.Q" > data.${N_ANC}.mean.rounded.Q

#prepare ALPHA
ALPHA=$(cat data.${N_ANC}.mean.rounded.Q | tr " " ",")

CFG_FILE=""
read -r -d '' CFG_FILE <<- ENDOFCFG
populations=$N_ANC
pfile=${PFILE#,}
genofile=data.geno
posfile=data.pos
outputancestryfile=ancestry.txt
offset=0.2
recombrate=1e-8
generations=$N_GEN
alpha=$ALPHA
ancestralsamplesize=$SRC_POP_SIZE
ldcutoff=0.1
ENDOFCFG
echo "$CFG_FILE" > "run.cfg"

