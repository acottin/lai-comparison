#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "i:o:c:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	c)
	    CONF_FILE=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

#for EXEC in ""
#do
#    if ! command -v $EXEC > /dev/null 2> /dev/null
#    then
#        echo "$EXEC not found, needed for formatting"
#    	exit 1
#    fi
#done

#------
# Output

#Saber already output file with the good format, so we just copy the data

bzip2 $DIR_IN/ind*_SABER_post_means.tab
cp -v $DIR_IN/ind*_SABER_post_means.tab.bz2 $DIR_OUT
rm $DIR_IN/ind*_SABER_post_means.tab.bz2

