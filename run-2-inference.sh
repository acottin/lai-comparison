#!/bin/bash
source "conf.sh"

module purge
module load bioinfo/elai/1.00
module load bioinfo/lamp/2.5
module load system/python/3.4.3

for EXP in $EXPERIENCES
do
  for VAL in ${VALUES[$EXP]}
  do
    for REP in $REPLICATION
    do
      for MET in $METHODS
      do
        JHOLD="format-$MET-$EXP-$VAL-$REP"
        JN="$MET-$EXP-$VAL-$REP"
        IN="$SIM/exp_$EXP/val_$VAL/rep_$REP"
        OUT="$INF/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
         case $MET in
           elai)
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
             "script/run-elai.sh -c $IN/out_param.sh -d $OUT"
             ;;
           saber)
	     module rm bioinfo/R/3.4.3
             module load bioinfo/R/2.15.2
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-saber.sh -c $IN/out_param.sh -d $OUT"
             ;;
           sabertf)
	     module rm bioinfo/R/3.4.3
             module load bioinfo/R/2.15.2
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-sabertf.sh -c $IN/out_param.sh -d $OUT"
             ;;
           mosaic)
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-mosaic.sh -c $IN/out_param.sh -d $OUT"
             ;;
           fmosaic)
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-fmosaic.sh -c $IN/out_param.sh -d $OUT"
             ;;
           vts)
             module rm bioinfo/R/2.15.2
	     module load bioinfo/R/3.4.3
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-vts.sh -c $IN/out_param.sh -d $OUT"
	     ;;
           lamp)
             qsub -hold_jid $JHOLD -q $SGE_Q -b yes -N $JN -V \
            "script/run-lamp.sh -c $IN/out_param.sh -d $OUT"
             ;;
         esac
      done
    done
  done
done

