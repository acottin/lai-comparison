#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "i:o:c:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	c)
	    CONF_FILE=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "perl" "mosaic_to_tab.pl"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Output

source $CONF_FILE

FILE_SNP_POS="$DIR_IN/data.snp.pos"
FILE_LIST=$DIR_IN/States_ind*
#get last MCMC run files by getting the biggest suffix of all output files
MAX=0
for FILE in $FILE_LIST
do
	#https://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash#965072
	TMP="${FILE##*.}"
	if [ $MAX -le $TMP ]
	then
		MAX=$TMP
	fi
done
FILE_LIST=$DIR_IN/States_ind*$MAX

mosaic_to_tab.pl -s $N_ANC -p $FILE_SNP_POS -o $DIR_OUT $FILE_LIST
