#!/usr/bin/env bash

#-----------
# Parameters

usage() {  echo "Usage: $0 [-d <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "d:c:" OPT
do
    case "$OPT" in
        d)
	    DIR=$OPTARG
	    ;;
	c)
	    CONF=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#----
# Run

source $CONF
cd $DIR

LABEL=10
CMD_PART=""
for ANC in $(seq $N_ANC)
do
    FILE="pop_${ANC}.geno"
    CMD_PART="$CMD_PART -g $FILE -p $LABEL"
    LABEL=$(expr $LABEL + 1)
done

LOWC=$(expr 5 "*" $N_ANC)

#for SEED in $(seq 1 5 )
#do
#  elai $CMD_PART -g pop_admx.geno -p 1 -pos pop_1.pos -C $N_ANC -c $LOWC -mg $N_GEN -s 30 -R $SEED
#done

elai $CMD_PART -g pop_admx.geno -p 1 -pos pop_1.pos -C $N_ANC -c $LOWC -mg $N_GEN -s 30 -R 1
