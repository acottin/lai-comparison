#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "awk"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

#data file
./bin/vcf_to_mosaic.awk $DIR_IN/data.vcf > $DIR_OUT/data.mosaic_input
#snp position file (Needed for output formatting)
grep -Pv '^#' $DIR_IN/data.vcf | cut -f 2 > $DIR_OUT/data.snp.pos
