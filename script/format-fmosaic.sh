#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>]" 1>&2; exit 1; }

while getopts "i:o:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

for EXEC in "awk"
do
    if ! command -v $EXEC > /dev/null 2> /dev/null
    then
        echo "$EXEC not found, needed for formatting"
    	exit 1
    fi
done

#------
# Input

#data file
./bin/vcf_to_mosaic.awk $DIR_IN/data.vcf > $DIR_OUT/data.mosaic_input
#get snp pos file
rm -f "$DIR_OUT/data.snp.pos"
ln -s $(readlink -f $DIR_IN/data.snp.pos) $DIR_OUT

#get a sample an haplotype sample
source "$DIR_IN/out_param.sh"
FIELDS=$(echo $SRC_POP_SIZE | awk '
BEGIN{ c = 1 }
{
  n = split($0, spl, ",")
  for(i in spl){
    min = c
    max = c + spl[i]
    res[i] = int((min + max) / 2)
    c = c + spl[i]
  }
}
END{
  #avoid trailing comma
  for(i=1;i<n;i++)
    printf("%d,", res[i])
  print(res[n])
}')

cut -f $FIELDS -d " " $DIR_OUT/data.mosaic_input | awk '
{
  for(i=1; i<NF ; i++)
    printf("%d ",$i+1)
  print($NF+1)
}' > $DIR_OUT/haplo_sample.mosaic_input
