#!/usr/bin/env bash

#source number
K="3"
#admixed number
A="2"
#total populatlion number
P=$(expr $K + $A)
#generation number
G="50"
#differentiation
D="0.20"
#sample size
S="20"
#Autofecondation
E="0.00 0.25 0.50 0.75 0.99"

for VE in $E
do
  DIR="val_e${VE}"
  mkdir -p $DIR
     
  #reprod file
  rm -f "$DIR/reprod.tab"
  #reprod elem, (at 0 before point)
  rate_i=$(echo 1-$VE | bc | sed 's/^\./0./')
  for i in $(seq 1 2); do echo "0.0/0.0/1.0/0.0:$G" >> "$DIR/reprod.tab";done
  echo "0.0/$VE/$rate_i/0.0:$G" >> "$DIR/reprod.tab"
  for i in $(seq 1 2); do echo "0.0/0.0/1.0/0.0:$G" >> "$DIR/reprod.tab";done
cat > "$DIR/contrib.tab" << EOL
300 0 0
0 300 0
0 0 300
100 100 100
100 100 100
EOL


cat > "$DIR/param.R" << EOL
# foundation
n_init_hap <- 1e3
n_hap_per_pop <- rep(3e2,3)
mut_rate <- 1e-8
chr_size <- 1e6 #1M
# reproduction steps
time_split <- c($D,$D)
n_sample_per_pop <- c(20,20,20,40,40)
pop_tag <- c(seq($K),rep("admx",$A))
EOL

done
