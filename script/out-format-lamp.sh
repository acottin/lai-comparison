#!/usr/bin/env bash
#-----------
# Parameters

usage() {  echo "Usage: $0 [-i <path>] [-o <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "i:o:c:" OPT
do
    case "$OPT" in
        i)
	    DIR_IN=$OPTARG
	    ;;
	o)
	    DIR_OUT=$OPTARG
	    ;;
	c)
	    CONF_FILE=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#------
# Check

#for EXEC in ""
#do
#    if ! command -v $EXEC > /dev/null 2> /dev/null
#    then
#        echo "$EXEC not found, needed for formatting"
#    	exit 1
#    fi
#done

#------
# Output

source $CONF_FILE

# we need to transpose the matrix, cut slab of it, and
# add a column for the SNP position

transpose () {
  awk '{
    for (i=1; i<=NF; i++){
      a[i,NR] = $i
      max = (max<NF?NF:max)
    }
  }
  END {
    for (i=1; i<=max; i++){
      for (j=1; j<=NR; j++) 
      printf "%s%s", a[i,j], (j<NR?OFS:ORS)
    }
  }'
}

COUNT=1
MIN=1
MAX=$N_ANC
N_SRC=$(echo $SRC_POP_SIZE | sed "s/,/+/g" | bc)
N_ADMX=$(expr $N_IND - $N_SRC)
N_LINE=$(expr $N_ADMX "*" $N_ANC)

#check empty file
if [ ! -s $DIR_IN/ancestry.txt ]
then
  echo $DIR_IN/ancestry.txt is empty !
  exit 1
fi

cat $DIR_IN/ancestry.txt | cut -f 2- > $DIR_IN/tmp.t

while [ $MAX -le $N_LINE ]
do
  #slice & transpose
  sed -n "${MIN},${MAX}p" $DIR_IN/tmp.t \
  | transpose                           \
  | paste -d " "$DIR_IN/data.pos -      \
  | bzip2 -c                            \
  > $DIR_OUT/"ind_$(printf "%05d" $COUNT).tab.bz2"

  echo "ind_$(printf "%05d" $COUNT).tab.bz2"
  MAX=$(expr $MAX + $N_ANC)
  MIN=$(expr $MIN + $N_ANC)
  COUNT=$(expr $COUNT + 1)
done

cd $DIR_IN
rm -f  tmp.t
