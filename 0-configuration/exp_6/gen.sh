#!/usr/bin/env bash

#source number
K="3"
#admixed number
A="10"
#total populatlion number
P=$(expr $K + $A)
#generation number
G="50"
#differentiation
D="0.20"
#sample size
S="20"
#Vegetative propagation
V="00 05 10 15 20 25 30 35 40 45"

DIR="val_vU"
mkdir -p $DIR

#contrib file
cat > "$DIR/contrib.tab" << EOL
300 0 0
0 300 0
0 0 300
EOL

#reprod file
rm -f "$DIR/reprod.tab"
#reprod elem, (at 0 before point)
for i in $(seq 1 3); do echo "0.0/0.0/1.0/0.0:$G" >> "$DIR/reprod.tab";done

for VV in $V
do
  #compute number of generation of veg. prop. for reprod.tab
  if [ "$VV" == "00" ]
  then
    echo "0/0/1/0:$G" >> "$DIR/reprod.tab"
  else
    ((GV=$G-VV))
    echo "0/0/1/0:$GV 1/0/0/0:$VV" >> "$DIR/reprod.tab"
  fi
  #add line for contrib.tab
  echo "100 100 100" >> "$DIR/contrib.tab"
done


cat > "$DIR/param.R" << EOL
# foundation
n_init_hap <- 1e3
n_hap_per_pop <- rep(3e2,3)
mut_rate <- 1e-8
chr_size <- 1e6 #1M
# reproduction steps
time_split <- c($D,$D)
n_sample_per_pop <- c(20,20,20,rep(10,10))
pop_tag <- c(seq($K),rep("admx",$A))
EOL
