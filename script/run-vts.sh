#!/usr/bin/env bash

#-----------
# Parameters

usage() {  echo "Usage: $0 [-d <path>] [-c <conf_file>]" 1>&2; exit 1; }

while getopts "d:c:" OPT
do
    case "$OPT" in
        d)
	    DIR=$OPTARG
	    ;;
	c)
	    CONF=$OPTARG
	    ;;
	\?)
	    usage
	    ;;
    esac
done

#----
# Run

source $CONF
cd $DIR

STEP1="ma"
NUM_AXES="2"

STEP2="clust"
NUM_THREAD=2
N_GRP=$(expr $N_ANC + 1)

STEP3="inf"
PLOIDY="2"


python3 ~/script/vcfHunter/bin/vcf2struct.1.0.py \
--vcf data.vcf             \
--names ancestor_names.txt \
--type FACTORIAL           \
--prefix $STEP1            \
--nAxes $NUM_AXES          \
--mulType coa              &&
python3 ~/script/vcfHunter/bin/vcf2struct.1.0.py \
--type SNP_CLUST-Kmean                        \
--nGroup $N_GRP                               \
--VarCoord ${STEP1}_variables_coordinates.tab \
--dAxes 1:$NUM_AXES                           \
--mat ma_matrix_4_PCA.tab                     \
--thread $NUM_THREAD                          \
--prefix $STEP2           &&
#-----------------------------------------------
#redo group.tab file to match tags from clustering,
#whith majority voting. The bigger value is UN, so we
#need to get the second bigger value for the group
N_SRC=$(echo $SRC_POP_SIZE | tr ',' '+' | bc)
function maj_vote_group {
  awk '
  { 
    a[$1]+=$2
  } 
  END{
    m1=m2=0;
    for(i in a){
      if(a[i]>a[m1]){
        m2=m1;m1=i
      } 
      else if(a[i]>a[m2]){
        m2=i
      }
    } 
  print m2 
  }'
}

rm -f grp.tmp

for I in $(seq $N_SRC)
do
  #move I cause file has 3 column
  J=$(expr $I + 3)
  cut -f 2,$J ${STEP2}_kMean_allele.tab \
  | tail -n +2     \
  | maj_vote_group \
  >> grp.tmp;
done
#combine file
tail -n +2 group.tab \
| cut -f 1 \
| paste - grp.tmp \
> group.fixed.tab

#----------------------------------------------
python3 ~/script/vcfHunter/bin/vcf2linear.1.1.py \
--vcf data.vcf                   \
--names all_names.txt            \
--namesH group.fixed.tab         \
--win 200                        \
--mat ${STEP2}_kMean_allele.tab  \
--gcol ${STEP2}_group_color.tab  \
--prefix $STEP3                  \
-T Binom                         \
-t $NUM_THREAD                   \
--ploidy $PLOIDY
