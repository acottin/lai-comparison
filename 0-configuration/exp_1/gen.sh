#!/usr/bin/env bash

#source number
K="3"
#admixed number
A="2"
#total populatlion number
P=$(expr $K + $A)
#generation number
G="05 10 20 30 40 50"
#differentiation
D="0.05 0.10 0.20 0.30 0.40"
#sample size
S="05 10 15 20 30 40"

for VG in $G
do
  for VD in $D
    do
    for VS in $S
    do
      DIR="val_g${VG}d${VD}s${VS}"
      mkdir -p $DIR
      
      #reprod file
      rm -f "$DIR/reprod.tab"
      for I in $(seq 1 5); do echo "0.0/0.0/1.0/0.0:$VG" >> "$DIR/reprod.tab"; done

cat > "$DIR/contrib.tab" << EOL
300 0 0
0 300 0
0 0 300
100 100 100
100 100 100
EOL


cat > "$DIR/param.R" << EOL
# foundation
n_init_hap <- 1e3
n_hap_per_pop <- rep(3e2,3)
mut_rate <- 1e-8
chr_size <- 1e6 #1M
# reproduction steps
time_split <- c($VD,$VD)
n_sample_per_pop <- c($VS,$VS,$VS,40,40)
pop_tag <- c(seq($K),rep("admx",$A))
EOL

    done
  done
done
