#!/bin/bash
source "conf.sh"

module purge
module load bioinfo/vcftools/0.1.14
module load bioinfo/plink/1.90b3v
module load bioinfo/admixture/1.23

local_Q="short.q"

for EXP in $EXPERIENCES
do
  for VAL in ${VALUES[$EXP]}
  do
    for REP in $REPLICATION
    do
      for MET in $METHODS
      do
        JHOLD="simu-${EXP}-${VAL}-${REP}"
        JN="format-$MET-$EXP-$VAL-$REP"
        IN="$SIM/exp_$EXP/val_$VAL/rep_$REP"
        OUT="$INF/exp_$EXP/met_$MET/val_$VAL/rep_$REP"
         case $MET in
           elai)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
             "script/format-elai.sh -i $IN -o $OUT "
             ;;
           saber)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-saber.sh -i $IN -o $OUT"
             ;;
           sabertf)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-sabertf.sh -i $IN -o $OUT"
             ;;
           mosaic)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-mosaic.sh -i $IN -o $OUT"
             ;;
           fmosaic)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-fmosaic.sh -i $IN -o $OUT"
             ;;
           vts)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-vts.sh -i $IN -o $OUT"
	     ;;
           lamp)
             qsub -hold_jid $JHOLD -q $local_Q -b yes -N $JN -V \
            "script/format-lamp.sh -i $IN -o $OUT"
             ;;
         esac
      done
    done
  done
done

